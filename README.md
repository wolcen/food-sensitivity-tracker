# Food Sensitivity Tracker

Finding causes of food sensitivity symptoms can be hard - this app aims to reduce the burden of that task. Discover food-to-symptoms correlations via a low-impedance interface.

## Design goals

The original goals of the project are simple - make it as easy as possible to determine correlations between food/food categories and symptoms. The primary features are:

* Ability to record customizable list of symptoms via either scale or an occurrence of a symptom.
* Low impedance: make it as easy as possible to supply the minimal amount of data needed to find correlations. For example, it should not require entry of weighted foods - just a simple food item itself.
* Flexible reporting: select symptoms to view, alongside selected food(s) or food groups (e.g. all nuts -> just almonds).

Note that "whole foods" eating will be the original focus, but as the food list is customizable, processed foods/food categories may also be entered. These will likely confound efforts to determine individual sources of sensitivities however, and tracking potentially problematic ingredients is likely recommended.

Foods to avoid will originally be built upon based on the [Autoimmune Protocol](https://autoimmunewellness.com/what-is-aip-the-definitive-guide/). This application would perhaps be most useful during a re-introduction phase from such a plan, but might also be useful while not eating as restrictively.